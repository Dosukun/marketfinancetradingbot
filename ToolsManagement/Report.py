class Report:
    # Class containing all data for strategy to take decision
    # Ichimoku data
    dict_ichimoku1 = {
        'Currency': None,
        'cloudColor': None,
        'priceInCloud': None,
        'posToCloud': None,
        'closeToCloud': None,
        'trend': None,
        'turningPoint': None,
        'close': None,
        'closeA': None,
        'closeB': None,
        'choice': None
    }
    # Ichimoku data
    dict_ichimoku2 = {
        'Currency': None,
        'cloudColor': None,
        'priceInCloud': None,
        'posToCloud': None,
        'closeToCloud': None,
        'trend': None,
        'turningPoint': None,
        'close': None,
        'closeA': None,
        'closeB': None,
        'choice': None
    }
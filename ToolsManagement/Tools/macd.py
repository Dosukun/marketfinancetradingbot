from ToolsManagement.Tools.ITools import ITools


class MacdTool(ITools):

    def __init__(self):
        super().__init__()

    @staticmethod
    def get_macd_data(data_fxcm, a, b, c):
        df = data_fxcm.copy()
        df["MA_Fast"] = df["Close"].ewm(span=a, min_periods=a).mean()
        df["MA_Slow"] = df["Close"].ewm(span=b, min_periods=b).mean()
        df["MACD"] = df["MA_Fast"] - df["MA_Slow"]
        df["Signal"] = df["MACD"].ewm(span=c, min_periods=c).mean()
        df.dropna(inplace=True)
        return df["MACD"], df["Signal"]

    def create_report(self, currency, candles, args):
        # TODO macd : CreateReport
        pass
from ToolsManagement.Tools.ITools import ITools
from ToolsManagement.Report import Report
import fxcmpy
import ctypes
import pandas as pd
import datetime as dt

class IchimokuTool():
    report = Report()
    defaultArgs = {
        'high': "High",
        'low': "Low",
        'open': "Open",
        'close': "Close",
        'tkp': 9,
        'kjp': 26,
        'skbp': 52,
        'decimalpip': 5,
    }

    last_Trend = None


    def __init__(self):
        super().__init__()

    def addToCandles(self, currency, candles):

      #  print("Loading ", currency)
        args = self.defaultArgs

        if "JPY" in currency:
            args['decimalpip'] = 3
        else:
            args['decimalpip'] = 5
        pd.set_option('mode.chained_assignment', None)
        # Adding the TenkanSen
        tenkan_max = candles['bidhigh'].rolling(window=args['tkp'], min_periods=0).max()
        tenkan_min = candles['bidlow'].rolling(window=args['tkp'], min_periods=0).min()
        candles["tenkan_sen"] = round((tenkan_max + tenkan_min) / 2, args['decimalpip'])

        # Kijun-sen (Base Line): (26-period high + 26-period low)/2))
        period26_high = candles['bidhigh'].rolling(window=args['kjp'], min_periods=0).max()
        period26_low = candles['bidlow'].rolling(window=args['kjp'], min_periods=0).min()
        candles['kijun_sen'] = round((period26_high + period26_low) / 2, args['decimalpip'])

        # Senkou Span A (Leading Span A): (Conversion Line + Base Line)/2))
        candles['senkou_span_a'] = round(((candles['tenkan_sen'] + candles['kijun_sen']) / 2),
                                         args['decimalpip']).shift(args['kjp'])

        # Senkou Span B (Leading Span B): (52-period high + 52-period low)/2))
        period52_high = candles['bidhigh'].rolling(window=args['skbp'], min_periods=0).max()
        period52_low = candles['bidlow'].rolling(window=args['skbp'], min_periods=0).min()
        candles['senkou_span_b'] = round(((period52_high + period52_low) / 2), args['decimalpip']).shift(args['kjp'])

        # The most current closing price plotted 26 time periods behind (optional)
        candles['chikou_span'] = candles['bidclose'].shift(-args['kjp'])
        self.create_report(currency, candles, args)
 #       print(candles)
        return self.report

    def create_report(self, currency, candles, args):
        self.report.dict_ichimoku1['Currency'] = currency
        # Set of position to cloud (Under, Above, Nothing) and closeToCmoud (distance with the cloud) in the case of senkou_span_a is higher that senkou_span_b
        if candles['senkou_span_b'].iloc[-1] < candles['senkou_span_a'].iloc[-1]:
            self.report.dict_ichimoku1['cloudColor'] = "Green"
            if candles['bidclose'].iloc[-1] < candles['senkou_span_b'].iloc[-1]:
                self.report.dict_ichimoku1['posToCloud'] = "Under"
                self.report.dict_ichimoku1['closeToCloud'] = abs(candles['bidclose'] - candles['senkou_span_b'].iloc[-1])
            elif candles['bidclose'].iloc[-1] > candles['senkou_span_a'].iloc[-1]:
                self.report.dict_ichimoku1['posToCloud'] = "Above"
                self.report.dict_ichimoku1['closeToCloud'] = abs(candles['bidclose'].iloc[-1] - candles['senkou_span_a'].iloc[-1])
            else:
                self.report.dict_ichimoku1['posToCloud'] = None
                self.report.dict_ichimoku1['closeToCloud'] = abs(candles['bidclose'].iloc[-1] - candles['senkou_span_a'].iloc[-1])
        # Set of position to cloud (Under, Above, Nothing) and closeToCmoud (distance with the cloud) in the case of senkou_span_a is lower that senkou_span_b
        elif candles['senkou_span_a'].iloc[-1] < candles['senkou_span_b'].iloc[-1]:
            self.report.dict_ichimoku1['cloudColor'] = "Red"
            if candles['bidclose'].iloc[-1] > candles['senkou_span_b'].iloc[-1]:
                self.report.dict_ichimoku1['posToCloud'] = "Above"
                self.report.dict_ichimoku1['closeToCloud'] = abs(candles['bidclose'].iloc[-1] - candles['senkou_span_b'].iloc[-1])
            elif candles['bidclose'].iloc[-1] < candles['senkou_span_a'].iloc[-1]:
                self.report.dict_ichimoku1['posToCloud'] = "Under"
                self.report.dict_ichimoku1['closeToCloud'] = abs(candles['bidclose'].iloc[-1] - candles['senkou_span_a'].iloc[-1])
            else:
                self.report.dict_ichimoku1['posToCloud'] = None
                self.report.dict_ichimoku1['closeToCloud'] = abs(candles['bidclose'].iloc[-1] - candles['senkou_span_b'].iloc[-1])
        # Set of position to cloud (Under, Above, Nothing) and closeToCmoud (distance with the cloud) in the case of senkou_span_a is equal to senkou_span_b
        else:
            if candles['bidclose'].iloc[-1] > candles['senkou_span_b'].iloc[-1]:
                self.report.dict_ichimoku1['posToCloud'] = "Above"
                self.report.dict_ichimoku1['closeToCloud'] = abs(candles['bidclose'].iloc[-1] - candles['senkou_span_b'].iloc[-1])
            if candles['bidclose'].iloc[-1] < candles['senkou_span_b'].iloc[-1]:
                self.report.dict_ichimoku1['posToCloud'] = "Under"
                self.report.dict_ichimoku1['closeToCloud'] = abs(candles['bidclose'].iloc[-1] - candles['senkou_span_b'].iloc[-1])
            else:
                self.report.dict_ichimoku1['posToCloud'] = None
                self.report.dict_ichimoku1['closeToCloud'] = abs(candles['bidclose'].iloc[-1] - candles['senkou_span_b'].iloc[-1])
        # Set a bool to true if the price is in the cloud
        self.report.dict_ichimoku1['priceInCloud'] = True if (candles['senkou_span_a'].iloc[-1] > candles['bidclose'].iloc[-1] > candles['senkou_span_b'].iloc[-1]) or (candles['senkou_span_a'].iloc[-1] < candles['bidclose'].iloc[-1] < candles['senkou_span_b'].iloc[-1]) else False
        # set if the trend is bullish or bearish
        if candles['tenkan_sen'].iloc[-1] > candles['kijun_sen'].iloc[-1]:
            self.report.dict_ichimoku1['trend'] = 'Bullish'
            if self.last_Trend == 'Bearish':
                self.last_Trend = 'Bullish'
                self.report.dict_ichimoku1['turningPoint '] = True
        elif candles['tenkan_sen'].iloc[-1] < candles['kijun_sen'].iloc[-1]:
            self.report.dict_ichimoku1['trend'] = 'Bearish'
            if self.last_Trend == 'Bullish':
                self.last_Trend = 'Bearish'
                self.report.dict_ichimoku1['turningPoint '] = True
        else:
            self.report.dict_ichimoku1['trend'] = None
        # check for unclear tendency
        self.report.dict_ichimoku1['close'] = False
        p = args['kjp']
        blocks = len(candles)
        last_chikou = candles['chikou_span'][blocks - p - 1]
        if candles['bidlow'][blocks - p - 1] <= last_chikou <= candles['bidhigh'][blocks - p - 1]:
            self.report.dict_ichimoku1['close'] = True
        # set a bool if the trend will change
        self.report.dict_ichimoku1['turningPoint'] = False
        ctick = abs(candles['tenkan_sen'][blocks - 2] - candles['kijun_sen'][blocks - 2])
        ctick2 = abs(candles['tenkan_sen'][blocks - 1] - candles['kijun_sen'][blocks - 1])
        if ctick * 10 ** args['decimalpip'] <= 2 and ctick2 * 10 ** args['decimalpip'] > 2:
            self.report.dict_ichimoku1['turningPoint'] = True
        # set of the last value of Close span_a and span_b
        self.report.dict_ichimoku1['closeA'] = candles['senkou_span_a'][blocks - 1]
        self.report.dict_ichimoku1['closeB'] = candles['senkou_span_b'][blocks - 1]

        #determinate what type of order we can open
        self.report.dict_ichimoku1['choice'] = None
        if self.report.dict_ichimoku1['cloudColor'] == "Red" and self.report.dict_ichimoku1['trend'] == 'Bearish' and self.report.dict_ichimoku1['posToCloud'] == "Under":
            self.report.dict_ichimoku1['choice'] = "Sell"
        if self.report.dict_ichimoku1['cloudColor'] == "Green" and self.report.dict_ichimoku1['trend'] == 'Bullish' and self.report.dict_ichimoku1['posToCloud'] == "Above":
            self.report.dict_ichimoku1['choice'] = "Buy"
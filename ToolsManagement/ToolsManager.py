import datetime
from dateutil.relativedelta import relativedelta
from ToolsManagement.Tools.IchimokuTool import IchimokuTool

class ToolsManager:
    """
    @type FinalReport: Report
    @type df: DataFrame
    @type df_dict: dict[[Dict[DataFrame], relativedelta]]
    @type com: FxcmCommunication
    """
    # Data about size and movement to get correct forex values
    df_dict = {
        "m1": [None, relativedelta(minutes=250), datetime.datetime.now() - relativedelta(minutes=1),
               relativedelta(minutes=1)],
        "m5": [None, relativedelta(minutes=1250), datetime.datetime.now() - relativedelta(minutes=5),
               relativedelta(minutes=5)],
        "m15": [None, relativedelta(minutes=3750), datetime.datetime.now() - relativedelta(minutes=15),
                relativedelta(minutes=15)],
        "m30": [None, relativedelta(minutes=7500), datetime.datetime.now() - relativedelta(minutes=30),
                relativedelta(minutes=30)],
        "H1": [None, relativedelta(hours=250), datetime.datetime.now() - relativedelta(hours=1),
               relativedelta(hours=1)],
        "H4": [None, relativedelta(hours=1000), datetime.datetime.now() - relativedelta(hours=4),
               relativedelta(hours=4)]
    }
    com = None

    def __init__(self, communication_instance):
        self.com = communication_instance

    # Method called to Update all data of tools
    def update_dfs(self, quotations):
        for key in self.df_dict.keys():
            self.df_dict[key][0] = self.prepare_candles(key, quotations)
        pass

    # Method to prepare the candles for Tools
    def prepare_candles(self, period, quotations):
        ret = {}
        now = datetime.datetime.now()
        for quot in quotations:
            stock = self.df_dict[period][2] + self.df_dict[period][3]
            if now < stock:
                return self.df_dict[period][0]
            start = now - self.df_dict[period][1]
            ret[quot] = self.com.con.get_candles(quot, period=period, start=start, stop=now)
        self.df_dict[period][2] = now
        return ret

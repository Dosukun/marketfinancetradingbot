from datetime import datetime
from dateutil.relativedelta import relativedelta
import pandas as pd


class BackTest:
    """@type ToolsManager: ToolsManager"""
    # Setup following data to setup Backtest
    startDate = datetime(2017, 2, 1, 15)
    endDate = datetime(2017, 8, 1)
    backtest_data_dir = '.\\BacktestData\\'
    minimum_movement = relativedelta(minutes=30)

    # Variable to select files and size of total data load
    # Variable to select cur load and the file prefix
    cur_dict = {
        "AUD/NZD": [{}, 'AUDNZD\\AUDNZD', None],
        "AUD/USD": [{}, 'AUDUSD\\AUDUSD', None],
        "EUR/USD": [{}, 'EURUSD\\EURUSD', None],
        "EUR/GBP": [{}, 'EURGBP\\EURGBP', None],
        "GBP/USD": [{}, 'GBPUSD\\GBPUSD', None],
        "NZD/USD": [{}, 'NZDUSD\\NZDUSD', None]
    }

    # Tools to chose the correct data to send to tools manager (current config is m30 H1 and H4)
    ToolsManager = None
    df_dict = {
        "m30": [None, relativedelta(minutes=7500), datetime.now() - relativedelta(minutes=30),
                relativedelta(minutes=30)],
        "H1": [None, relativedelta(hours=250), datetime.now() - relativedelta(hours=1), relativedelta(hours=1)],
        "H4": [None, relativedelta(hours=1000), datetime.now() - relativedelta(hours=4), relativedelta(hours=4)]
    }

    # Variable for the working of the system
    currentDate = startDate
    stop = False

    def __init__(self, tool_manager):
        self.ToolsManager = tool_manager
        for cur in self.cur_dict.keys():
            data_dict = self.init_data_dict(self.cur_dict[cur][1])
            for key in data_dict.keys():
                # Converting the index as date
                data_dict[key][0].index = pd.to_datetime(data_dict[key][0]['Time'])
                data_dict[key][3] = datetime(2016, 10, 25) - data_dict[key][1]
            self.cur_dict[cur][0] = data_dict.copy()
        pass

    def init_data_dict(self, begin_file):
        data_dict = {
            'm30': [None, relativedelta(minutes=+30), relativedelta(minutes=7500),
                    None, '_M30.csv'],
            'H1': [None, relativedelta(hours=+1), relativedelta(hours=250),
                   None, '_H1.csv'],
            'H4': [None, relativedelta(hours=+4), relativedelta(hours=1000),
                   None, '_H4.csv'],
        }
        for key in data_dict.keys():
            data_dict[key][0] = pd.read_csv(self.backtest_data_dir + begin_file + data_dict[key][4])
        return data_dict

    # Method to update tools manager's df_dict
    # noinspection PyTypeChecker, PyUnresolvedReferences
    def update_data(self):
        self.currentDate += self.minimum_movement
        if self.currentDate >= self.endDate:
            self.stop = True
        for cur_key in self.cur_dict:
            for key in self.cur_dict[cur_key][0].keys():
                start = self.currentDate - self.cur_dict[cur_key][0][key][2]
                stop = self.currentDate
                mask = (self.cur_dict[cur_key][0][key][0].index > start) & (self.cur_dict[cur_key][0][key][0].index <= stop)
                if self.df_dict[key][0] is None:
                    self.df_dict[key][0] = {}
                self.df_dict[key][0][cur_key] = self.cur_dict[cur_key][0][key][0].loc[mask].copy()
                self.cur_dict[cur_key][0][key][3] = self.currentDate
        self.ToolsManager.df_dict = self.df_dict.copy()
        print("##############################################\nFor date:\t", self.currentDate)

    # Method to close order if the stop or the limit is reached
    @staticmethod
    def check_stop_limit(last_max_reached, last_min_reached, opened_orders):
        for order in opened_orders:
            if order.buy:
                if order.limit is not 0 and order.limit < last_max_reached and order.stop is not 0 and order.stop > last_min_reached:
                    print("\tBad Possibility")
                    return
                if order.limit is not 0 and order.limit < last_max_reached:
                    order.close(order.limit)
                    print("\tTrade close by limit reached Currency: ", order.currency)
                    return
                if order.stop is not 0 and order.stop > last_min_reached:
                    order.close(order.stop)
                    print("\tTrade close by stop reached Currency: ", order.currency)
                    return
            if not order.buy:
                if order.stop is not 0 and order.stop < last_max_reached and order.limit is not 0 and order.limit > last_min_reached:
                    print("\tBad Possibility")
                    return
                if order.stop is not 0 and order.stop < last_max_reached:
                    order.close(order.stop)
                    print("\tTrade close by stop reached Currency: ", order.currency)
                    return
                if order.limit is not 0 and order.limit > last_min_reached:
                    order.close(order.limit)
                    print("\tTrade close by limit reached Currency: ", order.currency)
                    return

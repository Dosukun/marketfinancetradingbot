from OrdersManagement.OrdersManager import OrderManager
from ToolsManagement.ToolsManager import ToolsManager
from ToolsManagement.Tools.IchimokuTool import IchimokuTool
from ToolsManagement.Report import Report
import traceback
import pandas as pd


class Strategy:
    """
    Class description:
    @type GeneralOrderManager: OrderManager
    @type FinalReport: Report
    @type ToolsManager: ToolsManager
    @type AllCurrencyAvailable: list[str]
    """
    GeneralOrderManager = None
    ToolsManager = None
    bug = False
    FinalReport = None
    AllCurrencyAvailable = [""]

    def __init__(self, man, tools_manager, currency_available):
        self.ToolsManager = tools_manager
        self.GeneralOrderManager = man
        self.AllCurrencyAvailable = currency_available
        for currency in self.AllCurrencyAvailable:
            self.GeneralOrderManager.add_manager(currency)
        pass

    # noinspection PyBroadException
    def apply_strategy(self):  # TODO add shorts management and verify with the MACD
        try:
            """
            @type open_pos: list[Order]
            """
            self.GeneralOrderManager.get_all_opened_orders()
            # get ichimoku's analysed data on H1 (main period) and H4 (confirmation period)
            ichimoku_tool = IchimokuTool()
            main_periode = pd.DataFrame([], columns=['Currency', 'cloudColor', 'priceInCloud', 'posToCloud',
                                                     'closetoCloud', 'trend', 'turningPoint', 'close', 'closeA',
                                                     'closeB', 'choice'])
            confirmation = pd.DataFrame([], columns=['Currency', 'cloudColor', 'priceInCloud', 'posToCloud',
                                                     'closetoCloud', 'trend', 'turningPoint', 'close', 'closeA',
                                                     'closeB', 'choice'])
            for currency in self.AllCurrencyAvailable:
                self.FinalReport = ichimoku_tool.addToCandles(currency, self.ToolsManager.df_dict["H1"][0][currency]
                                                                .tail(104))
                main_periode = main_periode.append([self.FinalReport.dict_ichimoku1])
                self.FinalReport.dict_ichimoku2 = ichimoku_tool.addToCandles(currency,
                                                                               self.ToolsManager.df_dict["H4"][0][
                                                                                   currency].tail(104)).dict_ichimoku1
                confirmation = confirmation.append([self.FinalReport.dict_ichimoku2])
            main_periode = main_periode.sort_values(by=['choice', 'turningPoint', 'posToCloud'])
            confirmation = confirmation.sort_values(by=['choice', 'turningPoint', 'posToCloud'])

            # check currently opened positions
            print("======Check opened positions======")
            orders = self.GeneralOrderManager.get_all_opened_orders()
            for order in orders:
                if order.limit == 0:
                    order.modify_limit(4.0)
                cur = order.currency
                for index, main_row in main_periode.iterrows():
                    if main_row['Currency'] == cur:
                        if main_row['turningPoint']:
                            opened_position = order.buy
                            if main_row['choice'] is not None:
                                if (opened_position and main_row['choice'] == "Sell") or (
                                        opened_position is False and main_row['choice'] == "Buy"):
                                    order.close(self.ToolsManager.df_dict["H1"][0][main_row['Currency']].iloc[-1]["bidhigh"])
                                    print("\t\tTrade close by strategy currency: ", main_row['Currency'])

            # check for new trades
            for index, main_row in main_periode.iterrows():
                print("======Check for new trade in ", main_row['Currency'], "======")
                amount = 100
                if main_row['choice'] is not None:
                    if main_row['priceInCloud']:
                        print("======Exit: Price is in the cloud======")
                        continue

                    for idx, confirmation_row in confirmation.iterrows():
                        if confirmation_row['Currency'] == main_row['Currency'] and confirmation_row['choice'] == \
                                main_row['choice']:
                            amount *= 2  # if we have double confirmation, up investment amount
                    is_buy = main_row['choice'] == "Buy"

                    if main_row['closetoCloud']:
                        amount /= 2  # if price is close to cloud lowering investment
                    found = False
                    for order in orders:
                        if order.currency == main_row['Currency']:
                            found = True

                    if found is True or main_row['close'] is True:
                        print("======Exit: Order already exist======")
                        continue

                    # open a position with a stop loss and limit
                    if found is False:
                        stop = (main_row['closeB']) if main_row['posToCloud'] == "Above" else main_row['closeA']
                        print("\t\tTrade  opening with a stop at", -8, " for currency : ", main_row['Currency'])
                        self.GeneralOrderManager[main_row['Currency']].create_order(is_buy, amount,
                                                                                    self.ToolsManager.df_dict["H1"][0][
                                                                                        main_row['Currency']].iloc[-1][
                                                                                        "bidhigh"], stop=stop, limit=4)
                        print("======Exit======")

        except Exception:
            self.bug = True
            print(traceback.format_exc())

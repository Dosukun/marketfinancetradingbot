from fxcmpy import fxcmpy
import sys


class FxcmCommunication:
    con = None

    # Init class
    def __init__(self):
        if len(sys.argv) == 1:
            self.con = fxcmpy(config_file='fxcm.cfg')
        pass

    """
    buy == true we buy
    buy == false we sell
    """
    # Method to open an order.
    def open_order(self, buy, quotation, amount):
        if buy is True:
            order = self.con.create_market_buy_order(quotation, amount)
            self.con.change_trade_stop_limit(order.get_tradeId(), is_in_pips=True, is_stop=False, rate=4)
            self.con.change_trade_stop_limit(order.get_tradeId(), is_in_pips=True, is_stop=True, rate=4)
            return order
        else:
            order = self.con.create_market_sell_order(quotation, amount)
            self.con.change_trade_stop_limit(order.get_tradeId(), is_in_pips=True, is_stop=False, rate=4)
            self.con.change_trade_stop_limit(order.get_tradeId(), is_in_pips=True, is_stop=True, rate=4)
            return order

    """
    buy == true  we buy
    buy == false we sell
    stop lose et limit
    """
    # method to open an order with rules.
    def open_order_with_rules(self, buy, quotation, amount, stop, limit):
        return self.con.open_trade(symbol=quotation, is_buy=buy,
                                   rate=stop, is_in_pips=False,
                                   amount=amount, time_in_force='GTC',
                                   order_type='AtMarket', limit=limit, trailing_step=10)

    # Close an order return a confirmation bool.
    def close_at_id(self, trade_id, amount):
        test = self.con.close_trade(trade_id=trade_id, amount=amount)
        return test

    # Modify stop of order at the id order_id to put value as value and return a confirmation bool.
    def modify_stop_at_id(self, trade_id, value, is_in_pips=False):
        orders = self.con.get_open_trade_ids()
        return self.con.change_trade_stop_limit(trade_id, is_in_pips=is_in_pips,
                                                is_stop=True, rate=value)

    # Modify limit of order at the id order_id to put value as value and return a confirmation bool.
    def modify_limit_at_id(self, trade_id, value, is_in_pips=False):
        orders = self.con.get_open_trade_ids()
        return self.con.change_trade_stop_limit(trade_id, is_in_pips=is_in_pips,
                                                is_stop=False, rate=value)

    def get_all_opened_orders(self):
        return self.con.get_open_positions()

    def get_order_at_id(self, tradeId):
        try:
            ret = self.con.get_open_position(tradeId)
            return ret
        except:
            return None

    def get_all_closed_orders(self):
        return self.con.get_closed_trade_ids()
from Core.Core import Core
import sys


Core = Core()
if len(sys.argv) != 1:
    exit(Core.backtest())
else:
    exit(Core.loop())
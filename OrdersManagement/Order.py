# from Communication.OrdersCommunication import OrderCommunication
from fxcmpy.fxcmpy_order import fxcmpy_order
import sys


class Order:
    """
    @type Information: fxcmpy_order
    """
    Com = None
    Information = None

    amount = 0
    currency = ''
    TradeID = ''
    result = 0
    opened_price = 0
    buy = False
    Id = 0
    file = None
    limit = 0
    stop = 0
    logfile_name = ""
    close_bool = False
    closePrice = 0

    def __init__(self, order_com, buy, general_manager, system_id, quotation, amount, logfile, stop=0, limit=0, current_price=0, recup=False, tradeId=0, is_in_pips=False):
        self.buy = buy
        self.Com = order_com
        self.currency = quotation
        self.opened_price = current_price
        if len(sys.argv) == 1:
            if not recup:
                self.Information = self.Com.open_order(buy, quotation, amount)
                self.TradeID = self.Information.get_tradeId()
            else:
                self.TradeID = tradeId
                self.Information = self.Com.get_order_at_id(tradeId)
                if self.Information is None:
                    self.close()
        general_manager.add_index_to_dictionary(system_id, self)
        self.Id = system_id
        self.amount = amount
        if stop != 0:
            if not recup:
                self.modify_stop(stop, is_in_pips=False)
            else:
                self.stop = stop
        if limit != 0:
            if not recup:
                self.modify_limit(limit, is_in_pips=True)
            else:
                self.limit = limit
        if not recup:
            self.logfile_name = logfile
            self.file = open(self.logfile_name, 'a')
            printed_str = f"---\nOpen position :\n\tbuy price : {self.opened_price}\n\tbuy: {self.buy}\n\tamount: " \
                      f"{self.amount}\n\tlimit: {self.limit}\n\tstop: {self.stop}\n"
            self.file.write(printed_str)
            self.file.close()

    # Method called to close the order
    def close(self, price=0):
        if len(sys.argv) == 1:
            self.Com.close_at_id(self.TradeID, self.amount)
            self.close_bool = True
            return self.Information.get_accountId()
        else:
            self.closePrice = price
            if self.buy:
                self.result = (price - self.opened_price) * self.amount
            else:
                self.result = (self.opened_price - price) * self.amount
                print("\t", (self.opened_price - price) * self.amount)
        self.file = open(self.logfile_name, 'a')
        printed_str = f"---\nClose position :\n\tbuy price : {self.opened_price}\n\tbuy: {self.buy}\n\tamount: " \
                      f"{self.amount}\n\tlimit: {self.limit}\n\tstop: {self.stop}\n\tclose price: " \
                      f"{self.closePrice}\n"
        self.file.write(printed_str)
        self.file.close()
        self.close_bool = True

    # Method called to modify the stop
    def modify_stop(self, value, is_in_pips=False):
        if len(sys.argv) == 1:
            return self.Com.modify_stop_at_id(self.TradeID, value, is_in_pips=is_in_pips)
        if not self.buy:
            if is_in_pips:
                self.stop = self.opened_price + (self.opened_price * value / 100)
            else:
                self.stop = value
        else:
            if is_in_pips:
                self.stop = self.opened_price - (self.opened_price * value / 100)
            else:
                self.stop = value
        return True

    # Method called to modify the limit
    def modify_limit(self, value, is_in_pips=False):
        if len(sys.argv) == 1:
            return self.Com.modify_limit_at_id(self.TradeID, value, is_in_pips=is_in_pips)
        if not self.buy:
            if is_in_pips:
                self.limit = self.opened_price - (self.opened_price * value / 100)
            else:
                self.limit = value
        else:
            if is_in_pips:
                self.limit = self.opened_price + (self.opened_price * value / 100)
            else:
                self.limit = value
        return True
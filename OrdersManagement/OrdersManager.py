from multimethod import multimethod
from typing import Dict

from OrdersManagement.Order import Order


class OrderManager:
    """
    @type GeneralManager: OrderManager
    @type CurrentManager: OrderManager
    """
    Com = None
    GeneralManager = None
    OrderDictionary = {}  # type: Dict[int, Order]
    ManagerDictionary = {}  # type: Dict[str, OrderManager]
    CurrentManager = None
    next_id = 0
    logfile_name = ""

    # Initialization method , OrderCommunication get
    def __init__(self, order_com, general_manager, created_quotation, logfile):
        self.OrderDictionary = {}
        if general_manager is not None:
            self.GeneralManager = general_manager
        self.Com = order_com
        self.CurrentManager = created_quotation
        self.logfile_name = logfile

    # Close all order
    def close_all_orders(self, value1):
        for value in self.OrderDictionary.values():
            value.close_bool(value1)
        self.OrderDictionary = {}

    """
    buy is true 
    then we buy
    else we sell.
    Index:return
    """
    # Method to create an order
    def create_order(self, buy, amount, current_price, stop=0, limit=0, Receive=False, tradeId=0, is_in_pips=False):
        if self.GeneralManager is None:
            return -1
        new_order = Order(self.Com, buy, self.GeneralManager,
                          self.GeneralManager.next_id,
                          self.CurrentManager, amount, self.logfile_name, current_price=current_price, stop=stop, limit=limit, recup=Receive, tradeId=tradeId, is_in_pips=is_in_pips)
        self.OrderDictionary[self.GeneralManager.next_id] = new_order
        self.GeneralManager.next_id += 1
        self.GeneralManager.update_last_id()
        return self.next_id - 1

    # Method to update last id of all managers
    def update_last_id(self):
        for t in self.ManagerDictionary.values():
            t.next_id = self.next_id

    # Add a new manager (example add EUR/USD, EUR/JPY)
    def add_manager(self, string):
        self.ManagerDictionary[string] = OrderManager(self.Com, self, string, self.logfile_name)

    # Add the index to the order dictionary
    def add_index_to_dictionary(self, order_id, order):
        self.OrderDictionary[order_id] = order

    """
    if string : return the orderManager of the string (Example : EUR/USD, EUR/JPY)
    if int : return the order at the id
    """
    # movement in OrderManagers and Order
    @multimethod
    def __getitem__(self, key: int):
        """@rtype: Order"""
        return self.OrderDictionary[key]

    # movement in OrderManagers and Order
    @multimethod
    def __getitem__(self, key: str):
        """@rtype: OrderManager"""
        return self.ManagerDictionary[key]

    # Method to get all orders
    def get_all_orders(self):
        """@rtype: list[Order]"""
        return list(self.OrderDictionary.values())

    # Method to get all opened orders
    def get_all_opened_orders(self):
        """@rtype: list[Order]"""
        order_list = []
        self.update_local_from_fxcm()
        for o in self.OrderDictionary.values():
            if not o.close_bool:
                order_list.append(o)
        return order_list

    def update_local_from_fxcm(self):
        orders = self.Com.get_all_opened_orders()

        for index, row in orders.iterrows():
            cur = row['currency']
            if not self.ManagerDictionary[cur].OrderDictionary:
                self.ManagerDictionary[cur].create_order(row['isBuy'], row['amountK'], row['open'], row['stop'], row['limit'], Receive=True, tradeId=row['tradeId'])
        orders = self.Com.get_all_closed_orders()

        for order in self.OrderDictionary.values():
            if orders.count(int(order.TradeID)) > 0:
                if not order.close_bool:
                    order.close()
            pass

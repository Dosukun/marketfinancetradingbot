from Communication.FxcmCommunication import FxcmCommunication
from OrdersManagement.OrdersManager import OrderManager
from Strategy.Strategy import Strategy
from ToolsManagement.ToolsManager import ToolsManager
from Backtest.BackTest import BackTest
import time
import os


class Core:
    # Variable to modify to set the system
    AllCurrencyAvailable = ['EUR/USD', 'GBP/USD', "EUR/GBP", 'AUD/USD', 'AUD/NZD', 'NZD/USD']
    logfile_name = "bot_log_file.txt"

    # System tools
    FxcmComInstance = FxcmCommunication()
    GeneralManager = OrderManager(FxcmComInstance, None, None, logfile_name)
    ToolsManager = ToolsManager(FxcmComInstance)
    Strategy = Strategy(GeneralManager, ToolsManager, AllCurrencyAvailable)

    # Variable for the working of the system
    Exit = False

    def __init__(self):
        # if os.path.isfile(self.logfile_name):
        # os.remove(self.logfile_name)
        pass

    # Loop for the backtest system
    def backtest(self):
        backtest = BackTest(self.ToolsManager)
        self.GeneralManager.add_manager("EUR/USD")
        while not backtest.stop:
            backtest.update_data()
            backtest.check_stop_limit(self.ToolsManager.df_dict['m30'][0]["EUR/USD"].iloc[-1]["bidhigh"],
                                      self.ToolsManager.df_dict['m30'][0]["EUR/USD"].iloc[-1]["bidlow"],
                                      self.GeneralManager.get_all_opened_orders())
            self.Strategy.apply_strategy()
        self.add_total_to_file()

    # General Loop
    def loop(self):
        print('Bot open')
        # self.GeneralManager.add_manager("EUR/USD")
        # self.GeneralManager["EUR/USD"].create_order(1, True)
        start_time = time.time()
        while 1:
            try:
                self.update()
                time.sleep(60 - ((time.time() - start_time) % 60.0))
            except KeyboardInterrupt:
                print('\n\nKeyboard exception received. Exiting.')
                break
        print('Bot is close')
        # self.GeneralManager.close_all_orders()
        return 0

    # Method called by loop to update the system
    def update(self):
        self.ToolsManager.update_dfs(self.Strategy.AllCurrencyAvailable)
        self.Strategy.apply_strategy()

    # Method to get the total result of the backtest
    def get_back_test_result(self):
        """@rtype: float"""
        result = 0
        for order in self.GeneralManager.get_all_orders():
            result += order.result
        return result

    # Method to add the total to the log file
    def add_total_to_file(self):
        if os.path.isfile(self.logfile_name):
            file = open(self.logfile_name, 'a')
            file.write("Total : " + str(self.get_back_test_result()) + "\n")
            file.close()
